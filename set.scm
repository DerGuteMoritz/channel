(use extras srfi-69)

(define-record set table)

(define-record-printer (set s out)
  (fprintf out "#<set ~A>" (hash-table-size (set-table s))))

(define make-set
  (let ((super make-set))
    (lambda args
      (super (apply make-hash-table args)))))

(define (set-contains? set object)
  (hash-table-exists? (set-table set) object))

(define (set-insert! set object)
  (hash-table-set! (set-table set) object #t))

(define (set-insert-list! set objects)
  (for-each
   (lambda (object)
     (set-insert! set object))
   objects))

(define (set-remove! set object)
  (hash-table-delete! (set-table set) object))

(define (set-clear! set)
  (hash-table-clear! (set-table set)))

(define (set-members set)
  (hash-table-keys (set-table set)))

(define (set-fold set proc seed)
  (hash-table-fold
   (set-table set)
   (lambda (k v s) (proc k s))
   seed))

(define (set-empty? set)
  (zero? (hash-table-size (set-table set))))
